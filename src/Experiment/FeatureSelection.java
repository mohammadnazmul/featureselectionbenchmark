/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Experiment;

import cibm.attributeSelection.CMScore.CMScoreAttributeEval;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CorrelationAttributeEval;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.OneRAttributeEval;
import weka.attributeSelection.PrincipalComponents;
import weka.attributeSelection.Ranker;
import weka.attributeSelection.ReliefFAttributeEval;
import weka.attributeSelection.SymmetricalUncertAttributeEval;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author mohammad
 */
public class FeatureSelection {

    public static int nTopRankedFeature;
    public static boolean isDebug = true;
    public static Instances inputData;
    public static Instances outputData;
    public static int selTopRank = 100;
    public static String dataPath;
    public static String outPath;
    private static List<String> dataSets;
    private static List<String> inpDataPaths;
    private static List<String> outDataPaths;

    static void ParseSettingsFile(String fileName) {
        try {
            BufferedReader br = null;
            dataSets = new ArrayList<String>();

            File fin = new File(fileName);
            br = new BufferedReader(new FileReader(fin));
            String line = null;
            String delims = "[ \t:]+";

            while ((line = br.readLine()) != null) {
                if (line.startsWith("DataPath:") == true) {
                    String[] tokens = line.split(delims);
                    dataPath = tokens[1];
                } else if (line.startsWith("outPath:") == true) {
                    String[] tokens = line.split(delims);
                    outPath = tokens[1];
                } else if (line.startsWith("topNFeature:") == true) {
                    String[] tokens = line.split(delims);
                    nTopRankedFeature = Integer.parseInt(tokens[1]);
                } else if (line.isEmpty() == false) {
                    if (line.startsWith("#") == false) {
                        dataSets.add(line);
                    }
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Error!!! : Settings File Not Found at " + fileName);
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.err.println("Error!!! : Unable to Read Settings File at " + fileName);
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void DataPathBuilder() {
        inpDataPaths = new ArrayList<String>();
        outDataPaths = new ArrayList<String>();

        for (String strDS : dataSets) {
            if (strDS.length() > 0) {
                System.out.println(strDS);
                inpDataPaths.add(dataPath + "/" + strDS + "_train.arff");
                outDataPaths.add(outPath + "/" + strDS + "_train");
            }
        }
    }

    static Instances ReadArffFile(String txtFileName) {
        Instances data = null;
        try {
            DataSource source = new DataSource(txtFileName);
            data = source.getDataSet();
            data.setClassIndex(data.numAttributes() - 1);

            if (isDebug == true) {
                System.out.println("Data Loaded from: " + txtFileName);
                System.out.println("#Samples: " + data.numInstances() + "\t#Features: " + (data.numAttributes()-1) + "\t#Classes: " + data.numClasses());
            }
            return data;
        } catch (Exception ex) {
            System.err.println("Error!!!\n" + txtFileName + " not found.");
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    static void WriteArffFile(Instances data, String filePath) throws IOException {
        System.out.println("Saving to file " + filePath + "...\n");
        ArffSaver saver = new ArffSaver();
        saver.setInstances(data);
        saver.setFile(new File(filePath));
        saver.writeBatch();
    }

    protected static Instances retainFeatures(Instances data, String indices) {
        Instances selFeatureData = null;

        try {
            //System.out.println(indices);
            Remove remove = new Remove();
            remove.setAttributeIndices(indices);
            remove.setInvertSelection(true);
            remove.setInputFormat(data);
            selFeatureData = Filter.useFilter(data, remove);
        } catch (Exception ex) {
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return selFeatureData;
    }

    protected static Instances FSCMScore(Instances data, int cmN, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();
        String nf = "" + nTopRankedFeature;
        String cm = "" + cmN;
        // Step 1: setEvaluator
        String[] cmOptions = {"-C", cm, "N", nf};
        CMScoreAttributeEval eval = new CMScoreAttributeEval();
        eval.setOptions(cmOptions);
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);
        attSelection.SelectAttributes(data);

        double[][] ranks = attSelection.rankedAttributes();
        /*        for (int i = 0; i < ranks.length; i++) {
         System.out.println(ranks[i][0] + ", " + ranks[i][1]);
         }
         */
        int topPos = 0;
        int mid = nTopRankedFeature / 2;
        if (nTopRankedFeature % 2 == 1) {
            topPos = mid + 1;
        } else {
            topPos = mid;
        }
        int startNeg = ranks.length - mid;

        String indices = "";
        for (int i = 0; i < topPos; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }

        for (int i = startNeg; i < ranks.length; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
        System.out.println("After Applying CM" + cm + " Score Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    protected static Instances FSInfoGain(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);

        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);

        //selFeatureData = attSelection.reduceDimensionality(data);
        System.out.println("After Applying Information Gain Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    protected static Instances FSGainRatio(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        GainRatioAttributeEval eval = new GainRatioAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);
        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
        //selFeatureData = attSelection.reduceDimensionality(data);

        System.out.println("After Applying Gain Ratio Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    protected static Instances FSCorrelation(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        CorrelationAttributeEval eval = new CorrelationAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);
        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
        //selFeatureData = attSelection.reduceDimensionality(data);

        System.out.println("After Applying Correlation Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    protected static Instances FSOneR(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        OneRAttributeEval eval = new OneRAttributeEval();
        String strOROption = "-S 1 -F 10 -B 6";
        String[] ORoptions = Utils.splitOptions(strOROption);
        eval.setOptions(ORoptions);
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);
        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
        //selFeatureData = attSelection.reduceDimensionality(data);

        System.out.println("After Applying OneR Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    protected static Instances FSPrincipalComponents(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        PrincipalComponents eval = new PrincipalComponents();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);
        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
        //selFeatureData = attSelection.reduceDimensionality(data);

        System.out.println("After Applying Principal Components Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;

    }

    protected static Instances FSRelief(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        ReliefFAttributeEval eval = new ReliefFAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);
        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
//selFeatureData = attSelection.reduceDimensionality(data);

        System.out.println("After Applying Relief Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    /*
     protected static Instances FSCM1(Instances data) throws Exception {
     Instances selFeatureData = null;
     CMScore cm1 = new CMScore(data);
     String nf = "" + nTopRankedFeature;

     String[] cmOptions = {"-c", "1", "-f", "T", "-n", nf};

     cm1.SetOptions(cmOptions);
     int[] selFeatureIdx = cm1.selectedAttributeIndex();

     String featureIndices = "";
     for (int i = 0; i < nTopRankedFeature; i++) {
     String fi = "" + selFeatureIdx[i];
     featureIndices += (fi + ",");
     }
     //featureIndices += data.numAttributes();

     Remove remove = new Remove();
     remove.setAttributeIndices(featureIndices);
     remove.setInvertSelection(true);
     remove.setInputFormat(data);
     selFeatureData = Filter.useFilter(data, remove);
     selFeatureData.setRelationName(data.relationName());
     selFeatureData.setClassIndex(selFeatureData.numAttributes() - 1);

     System.out.println("After Applying CM1 Feature Selection..");
     System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

     //Return Dataset with selected features
     return selFeatureData;
     }

     protected static Instances FSCM2(Instances data) throws Exception {
     Instances selFeatureData = null;
     CMScore cm2 = new CMScore(data);
     String nf = "" + nTopRankedFeature;
     String[] cmOptions = {"-c", "2", "-f", "T", "-n", nf};

     cm2.SetOptions(cmOptions);
     int[] selFeatureIdx = cm2.selectedAttributeIndex();

     String featureIndices = "";
     for (int i = 0; i < selFeatureIdx.length; i++) {
     String fi = "" + selFeatureIdx[i];
     featureIndices += (fi + ",");
     }
     featureIndices += data.classIndex();

     Remove remove = new Remove();
     remove.setAttributeIndices(featureIndices);
     remove.setInvertSelection(true);
     remove.setInputFormat(data);
     selFeatureData = Filter.useFilter(data, remove);
     selFeatureData.setRelationName(data.relationName());
     selFeatureData.setClassIndex(selFeatureIdx.length);

     System.out.println("After Applying CM2 Feature Selection..");
     System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

     //Return Dataset with selected features
     return selFeatureData;
     }
     */
    protected static Instances FSSymmetricalUncert(Instances data, String[] rankSearchOptions) throws Exception {
        Instances selFeatureData = null;

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        SymmetricalUncertAttributeEval eval = new SymmetricalUncertAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        rank.setOptions(rankSearchOptions);
        attSelection.setSearch(rank);

        // Step 3: SelectAttributes
        attSelection.SelectAttributes(data);
        double[][] ranks = attSelection.rankedAttributes();
        String indices = "";
        for (int i = 0; i < nTopRankedFeature; i++) {
            indices += (((int) ranks[i][0] + 1) + ",");
        }
        indices += (data.classIndex() + 1);

        selFeatureData = retainFeatures(data, indices);
        //selFeatureData = attSelection.reduceDimensionality(data);

        System.out.println("After Applying Symmetrical Uncertainity Feature Selection..");
        System.out.println("#Samples: " + selFeatureData.numInstances() + "\t#Features: " + (selFeatureData.numAttributes()-1) + "\t#Classes: " + selFeatureData.numClasses());

        //Return Dataset with selected features
        return selFeatureData;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        System.out.println("\nReading Settings File...");
        String inputFile = Utils.getOption("i", args);
        System.out.println(inputFile);

        System.out.println("\nParsing Settings from File...");
        ParseSettingsFile(inputFile);
        DataPathBuilder();

        String nRank = "" + nTopRankedFeature;
        String[] options = {"-T", "-1.7976931348623157E308", "-N", nRank};

        System.out.println("\nStarting Feature Selections...");

        String filename = outPath + "/FS-Top-" + nTopRankedFeature + "-CPUTimes.txt";
        FileWriter fw = new FileWriter(filename, true); //the true will append the new data
        fw.write("Dataset\tCM1\tCM2\tIG\tGR\tCR\tOR\tRF\tSU\n");//appends the string to the file
        //fw.write("Dataset\tIG\tGR\tCR\tOR\tPC\tRF\tSU\n");

        fw.close();

        int idx = 0;
        long startTimeMs, taskTimeMs;
        for (String inpDSFile : inpDataPaths) {
            inputData = ReadArffFile(inpDSFile);
            System.out.println("\n\nDataSet: " + inpDSFile);
            String relName = dataSets.get(idx) + "-" + nTopRankedFeature;
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(dataSets.get(idx) + "\t");//appends the string to the file
            fw.close();

            // CM1
            int cmN = 1;
            startTimeMs = System.currentTimeMillis();
            Instances dataCM1 = FSCMScore(inputData, cmN, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataCM1.setRelationName(relName);
            WriteArffFile(dataCM1, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_CM" + cmN + ".arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            // CM2
            cmN = 2;
            startTimeMs = System.currentTimeMillis();
            Instances dataCM2 = FSCMScore(inputData, cmN, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataCM2.setRelationName(relName);
            WriteArffFile(dataCM2, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_CM" + cmN + ".arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            // IG
            startTimeMs = System.currentTimeMillis();
            Instances dataIG = FSInfoGain(inputData, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataIG.setRelationName(relName);
            WriteArffFile(dataIG, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_IG.arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            // GR
            startTimeMs = System.currentTimeMillis();
            Instances dataGR = FSGainRatio(inputData, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataGR.setRelationName(relName);
            WriteArffFile(dataGR, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_GR.arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            // CR
            startTimeMs = System.currentTimeMillis();
            Instances dataCR = FSCorrelation(inputData, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataCR.setRelationName(relName);
            WriteArffFile(dataCR, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_CR.arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            // OR
            startTimeMs = System.currentTimeMillis();
            Instances dataOR = FSOneR(inputData, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataOR.setRelationName(relName);
            WriteArffFile(dataOR, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_OR.arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            /*            
             startTimeMs = System.currentTimeMillis( );
             Instances dataPC = FSPrincipalComponents(inputData);
             taskTimeMs  = System.currentTimeMillis( ) - startTimeMs;
             dataPC.setRelationName(relName);
             WriteArffFile(dataPC, outDataPaths.get(idx) + "_top_"+nTopRankedFeature+"_PC.arff");
             fw = new FileWriter(filename, true); //the true will append the new data
             fw.write(taskTimeMs + "\t");//appends the string to the file
             fw.close();
             */
            // RF
            startTimeMs = System.currentTimeMillis();
            Instances dataRF = FSRelief(inputData, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataRF.setRelationName(relName);
            WriteArffFile(dataRF, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_RF.arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\t");//appends the string to the file
            fw.close();

            // SU
            startTimeMs = System.currentTimeMillis();
            Instances dataSU = FSSymmetricalUncert(inputData, options);
            taskTimeMs = System.currentTimeMillis() - startTimeMs;
            dataSU.setRelationName(relName);
            WriteArffFile(dataSU, outDataPaths.get(idx) + "_top_" + nTopRankedFeature + "_SU.arff");
            fw = new FileWriter(filename, true); //the true will append the new data
            fw.write(taskTimeMs + "\n");//appends the string to the file
            fw.close();

            idx++;
        }
        System.out.println("\nDone!");
    }

}
